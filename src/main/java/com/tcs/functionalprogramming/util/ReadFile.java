package com.tcs.functionalprogramming.util;

import com.tcs.functionalprogramming.code.User;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.stream.Stream;

public class ReadFile {

    final static Logger LOGGER = LoggerFactory.getLogger(ReadFile.class);

    public static ArrayList<User> loadFile(){

        ArrayList<User> listUsers = new ArrayList<>();

        LOGGER.info("Inicio lectura del archivo data.txt");

        ResourceLoader resourceLoader = new DefaultResourceLoader();
        Resource resource = resourceLoader.getResource("classpath:data.txt");

        try{
            File file = new File(resource.getFile().toURI());
            Stream<String> linesStream = Files.lines(file.toPath());

            linesStream.forEach(line -> {

                LOGGER.info("Line Data TxT: {}", line);

                /*
                Generar Código para transformación de cada línea de texto a un
                objeto de tipo User.
                 */

            });
        }
        catch (IOException e) {
            LOGGER.error("IOException: ", e.getMessage());
        }

        return listUsers;
    }
}
