package com.tcs.functionalprogramming;

import com.tcs.functionalprogramming.code.User;
import com.tcs.functionalprogramming.util.ReadFile;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;

@SpringBootApplication
public class FunctionalProgrammingApplication{

	public static void main(String[] args){

		SpringApplication.run(FunctionalProgrammingApplication.class, args);

		ArrayList<User> df = ReadFile.loadFile();
	}
}
